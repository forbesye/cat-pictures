const getCat = async () => {
	const response = await fetch("https://cataas.com/cat?json=true");
	const cat = await response.json()
	document.getElementById('catImg').src = "https://cataas.com" + cat.url;
}